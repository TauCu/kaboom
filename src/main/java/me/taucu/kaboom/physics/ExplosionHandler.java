package me.taucu.kaboom.physics;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.compatibility.APIAccess;
import me.taucu.kaboom.compatibility.VersionSupport;
import me.taucu.kaboom.events.DebrisBreakEvent;
import me.taucu.kaboom.events.DebrisLandEvent;
import me.taucu.kaboom.events.ExplosionHandledEvent;
import me.taucu.kaboom.events.HandleExplosionEvent;
import me.taucu.kaboom.util.DataSerializer;
import me.taucu.kaboom.util.LocationUtil;
import me.taucu.kaboom.util.RayBouncer;
import me.taucu.kaboom.util.RayBouncer.RayBounceResult;
import me.taucu.kaboom.util.Vectors;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.TileState;
import org.bukkit.block.data.BlockData;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Vector;

import java.io.Closeable;
import java.util.*;
import java.util.logging.Level;

public class ExplosionHandler implements Listener, Closeable {
    
    private static final BlockData AIR_DATA = Material.AIR.createBlockData();
    private static final byte[] VISUAL_ONLY_DATA = new byte[] {(byte) (DataSerializer.DATA_VERSION >>> 24), (byte) (DataSerializer.DATA_VERSION >>> 16), (byte) (DataSerializer.DATA_VERSION >>> 8), (byte) (DataSerializer.DATA_VERSION)};
    private static final Random rand = new Random();

    private final HashSet<Material> disabledFallingBlocks = new HashSet<Material>();
    private final HashSet<Material> disabledFallingBlocksPlacement = new HashSet<Material>();
    private final HashSet<Material> disabledFallingBlockDrops = new HashSet<Material>();

    private final ItemStack tool = new ItemStack(Material.DIAMOND_PICKAXE);

    private final KaBoom kaboom;
    private final APIAccess api = VersionSupport.get();
    private final NamespacedKey fallingDataKey;

    private BlockChangeSender blockChangeSender;

    private boolean silkTouch = false;
    private int fortune = 0;

    private int maxRayAttempts = 5;
    private int maxRayDistance = 10;
    private double breakChanceNoFree = 80;
    private int maxDebrisInSimulationDistance = 1200;

    private double velocityMultiplier = 2;
    private double velocityRandom = 0.2;
    
    private boolean fallingBlockBreakAnimation = true;
    private boolean fallingBlocksHurtEntities = true;
    private boolean fallingBlockVisualOnlyMode = false;

    private double damageBaseOccluding, damageBaseNonOccluding, damageMultiplierBlast, damageMultiplierHardness;

    private long explodedBlocks = 0;

    public ExplosionHandler(KaBoom pl) {
        this.kaboom = pl;
        fallingDataKey = new NamespacedKey(pl, "DebrisData");
        this.blockChangeSender = new BlockChangeSender(pl, 5);
    }

    /**
     * Called to modify entity/block explosion events and will not work properly by itself
     *
     * @param center The center {@link Location} of the blast
     * @param toBlow {@link List} of blocks that are to be blown up
     * @apiNote This method exists for legacy compatibility
     * 
     * @see ExplosionHandler#handleExplosion(Location, List, float, boolean, Object)
     */
    public void handleExplosion(Location center, List<Block> toBlow) {
        handleExplosion(center, toBlow, 1.0F, true, null);
    }
    
    /**
     * Called to modify entity/block explosion events and will not work properly by itself
     * 
     * @param center The center {@link Location} of the blast
     * @param toBlow {@link List} of blocks that are to be blown up
     * @param yield The chance (0.0-1.0) that we should drop an item from a block
     * @param createDebris If we should attempt to create debris at all
     * @param source The block or entity source of the explosion (nullable)
     */
    public void handleExplosion(Location center, List<Block> toBlow, float yield, boolean createDebris, Object source) {
        HandleExplosionEvent hee = new HandleExplosionEvent(center.getWorld(), center, toBlow, yield, createDebris, source);
        if (!hee.callEvent()) return;
        center = hee.getCenter();
        toBlow = hee.getToBlow();
        yield = hee.getYield();
        createDebris = hee.createsDebris();
        source = hee.getSource();

        Vector centerVec = center.toVector();
        HashSet<Block> allBlocks = new HashSet<>(toBlow);
        toBlow.clear();

        List<Block> toBreak = new ArrayList<>(allBlocks.size());
        List<FallingBlock> allDebris = new ArrayList<>(allBlocks.size());

        int debrisBudget = getDebrisBudget(center);

        for (Block block : allBlocks) {
            Material type = block.getType();
            if (api.shouldBlow(type)) {
                toBlow.add(block);
            } else if (!createDebris || block.getState() instanceof TileState || allDebris.size() >= debrisBudget || disabledFallingBlocks.contains(type)) {
                toBreak.add(block);
            } else {
                FallingBlock debris = createDebris(centerVec, block, type, allBlocks, yield, breakChanceNoFree);
                if (debris == null) {
                    toBreak.add(block);
                } else {
                    allDebris.add(debris);
                }
            }
        }

        new ExplosionHandledEvent(center.getWorld(), this, center, allBlocks, toBreak, toBlow, allDebris, yield, source)
                .callEvent();

        for (Block block : toBreak) {
            if ((yield >= 1 || rand.nextFloat() < yield)) {
                api.breakBlock(block, tool);
            } else {
                block.setBlockData(AIR_DATA, true);
            }
        }

        this.explodedBlocks += (toBreak.size() + toBlow.size() + allDebris.size());
    }

    /**
     * Used to get the budget of how many debris can be created in an area
     * @param loc the location where the budget should be computed
     * @return the budget, or 0 if budget is full
     */
    public int getDebrisBudget(Location loc) {
        World world = loc.getWorld();
        int view = world.getViewDistance() + 1 << 4;
        return Math.max(0, maxDebrisInSimulationDistance - world.getNearbyEntities(loc, view * 2, Double.MAX_VALUE, view * 2, this::isDebris).size());
    }

    /**
     * Used internally to create debris also sets the target block to air if successful
     * @param centerVec the center of the explosion (midpoint of block)
     * @param block the {@link Block} to create debris for
     * @param type the {@link Material} of the block (optimization to reduce getType calls)
     * @param ignoreInTrace the {@link HashSet<Block>} of blocks to ignore in raytrace operations
     * @param breakChanceNoFree The chance (0.0-100.0) that the debris should break if rayBouncing fails to find a free trajectory
     * @return the {@link FallingBlock} debris or null
     *
     * @apiNote This method exists for legacy compatibility
     *
     * @see ExplosionHandler#createDebris(Vector, Block, Material, HashSet, float, double)
     */
    public FallingBlock createDebris(Vector centerVec, Block block, Material type, HashSet<Block> ignoreInTrace, double breakChanceNoFree) {
        return createDebris(centerVec, block, type, ignoreInTrace, 1.0F, breakChanceNoFree);
    }

    /**
     * Used internally to create debris also sets the target block to air if successful
     * @param centerVec the center of the explosion (midpoint of block)
     * @param block the {@link Block} to create debris for
     * @param type the {@link Material} of the block (optimization to reduce getType calls)
     * @param ignoreInTrace the {@link HashSet<Block>} of blocks to ignore in raytrace operations
     * @param yield The chance (0.0-1.0) that we should drop an item from the debris
     * @param breakChanceNoFree The chance (0.0-100.0) that the debris should break if rayBouncing fails to find a free trajectory
     * @return the {@link FallingBlock} debris or null
     */
    public FallingBlock createDebris(Vector centerVec, Block block, Material type, HashSet<Block> ignoreInTrace, float yield, double breakChanceNoFree) {
        Collection<ItemStack> drops = block.getDrops(tool);
        BlockData fallingBlockData;
        // if it only has one drop and that drop is not itself and is also a block, set the type of the falling block to that type
        if (drops.size() == 1) {
            Material dropType = drops instanceof List ? ((List<ItemStack>) drops).get(0).getType() : drops.iterator().next().getType();
            if (dropType != type && dropType.isBlock() && !api.isAir(dropType)) {
                fallingBlockData = Bukkit.createBlockData(dropType);
            } else {
                fallingBlockData = block.getBlockData();
            }
        } else {
            fallingBlockData = block.getBlockData();
        }

        Location centerLoc = LocationUtil.toCenterLocation(block.getLocation());
        Vector velocity = calculateVelocity(centerVec, centerLoc);

        drops = (yield >= 1 || rand.nextFloat() < yield) ? drops : Collections.emptyList();

        RayBounceResult bounceResult;
        int times = 0;
        do {
            bounceResult = RayBouncer.rayBounce(ignoreInTrace, centerLoc, Vectors.randomVector(velocityRandom, velocityRandom).add(velocity), 2, maxRayDistance);
        } while (!bounceResult.foundFreeVector && ++times < maxRayAttempts);

        if (breakChanceNoFree > 0 && !bounceResult.foundFreeVector && rand.nextDouble() * 100 < breakChanceNoFree) {
            return null;
        } else {
            block.setBlockData(AIR_DATA, true);
            FallingBlock debris = spawnFallingBlock(
                    centerFallingBlock(bounceResult.location),
                    fallingBlockData,
                    bounceResult.velocity
            );
            if (fallingBlockVisualOnlyMode) {
                debris.getPersistentDataContainer().set(fallingDataKey, PersistentDataType.BYTE_ARRAY, VISUAL_ONLY_DATA);
                dropItems(debris, drops);
            } else {
                debris.getPersistentDataContainer().set(fallingDataKey, PersistentDataType.BYTE_ARRAY, DataSerializer.serializeDebrisData(drops));
            }
            return debris;
        }
    }
    
    /**
     * Used internally to calculate the correct velocity away from the explosion source
     * @param centeredSourceVec the centered source vector
     * @param centeredBlockLoc the centered target block location
     * @return the calculated vector
     */
    public Vector calculateVelocity(Vector centeredSourceVec, Location centeredBlockLoc) {
        Vector centeredBlockVec = centeredBlockLoc.toVector();
        if (centeredSourceVec.equals(centeredBlockVec)) {
            return new Vector(0, 1, 0).multiply(-velocityMultiplier);
        } else {
            return centeredSourceVec.clone()
                    .subtract(centeredBlockVec)
                    .normalize()
                    .multiply(-velocityMultiplier);
        }
    }
    
    /**
     * Spawns a {@link FallingBlock}
     * 
     * @param loc      {@link Location} to spawn the {@link FallingBlock}
     * @param data     {@link BlockData} of the {@link FallingBlock}
     * @param velocity A {@link Vector} representing the velocity of the
     *                 {@link FallingBlock}
     * @return The {@link FallingBlock}
     */
    public FallingBlock spawnFallingBlock(Location loc, BlockData data, Vector velocity) {
        @SuppressWarnings("deprecation")
        FallingBlock falling = loc.getWorld().spawnFallingBlock(loc, data);
        falling.setPersistent(true);
        falling.setVelocity(velocity);
        //System.out.println(data.getMaterial());
        //falling.getLocation().getWorld().spawnParticle(Particle.BLOCK_MARKER, falling.getLocation().add(0, 0.5, 0), 1, data);
        return falling;
    }
    
    /**
     * Centers a {@link Location} for {@link FallingBlock}s relative to the world
     * grid.
     * 
     * @param loc The location to center
     * @return The {@link FallingBlock} specific centered {@link Location}
     */
    public Location centerFallingBlock(Location loc) {
        loc.setX(loc.getBlockX() + 0.5);
        loc.setY(loc.getY() + 0.0014); // special offset for falling sand 0.0014
        loc.setZ(loc.getBlockZ() + 0.5);
        return loc;
    }

    /**
     * Drops all items naturally in the world if each item isItem() and is not air
     * @param src the debris to drop the items from
     * @param itemStacks the items to drop
     */
    public void dropItems(FallingBlock src, Collection<ItemStack> itemStacks) {
        Location loc = src.getBoundingBox().getCenter().toLocation(src.getWorld()).subtract(0.5, 0.5, 0.5);
        for (ItemStack is : itemStacks) {
            Material type = is.getType();
            if (type.isItem() && !api.isAir(type)) {
                loc.getWorld().dropItemNaturally(loc, is);
            }
        }
    }

    /**
     * Safely deserializes the data of falling block debris
     *
     * @param serialized the serialized data
     * @return an ArrayList of ItemStacks or an empty ArrayList if an error occurred
     */
    public ArrayList<ItemStack> deserializeDebrisStacks(byte[] serialized) {
        try {
            return DataSerializer.deserializeDebrisData(serialized, 1000);
        } catch (Exception ex) {
            kaboom.getLogger().log(Level.SEVERE, "Error while deserializing items. data=\"" + (serialized == null ? "null" : Base64.getEncoder().encodeToString(serialized)) + "\"", ex);
        }
        return new ArrayList<>();
    }
    
    /**
     * checks if this debris was created in visual mode
     * @param data the data to check
     * @return true if visual, false otherwise
     */
    public static boolean isVisualMode(byte[] data) {
        return VISUAL_ONLY_DATA.length == data.length
                && VISUAL_ONLY_DATA[3] == data[3] && VISUAL_ONLY_DATA[2] == data[2] && VISUAL_ONLY_DATA[1] == data[1] && VISUAL_ONLY_DATA[0] == data[0];
    }
    
    /**
     * Called after derbies land to apply animations etc
     * @param fb the falling block
     * @param didBreak If the falling block broke when it landed
     */
    public void postFall(FallingBlock fb, boolean didBreak) {
        Location loc = fb.getLocation();
        BlockData data = fb.getBlockData();
        VersionSupport.get().playBreakSound(loc, data);
        loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_ATTACK_CRIT, SoundCategory.BLOCKS, 0.25F, 0.8F);
        if (didBreak && fallingBlockBreakAnimation) {
            loc.getWorld().spawnParticle(api.getBlockDustParticle(), loc.add(0, 0.5, 0), 50, 0.25, 0.25, 0.25, data);
        }
        if (fallingBlocksHurtEntities) {
            BoundingBox box = fb.getBoundingBox();
            // World#getNearbyEntities doesn't calculate intersections properly, so we need to make a fuzzy bounding box
            // ... because of course we do
            BoundingBox fuzzyBox = box.clone().expand(3, 3, 3);
            for (Entity ent : loc.getWorld().getNearbyEntities(fuzzyBox)) {
                // we don't want to squash items as it may squash its own item.
                if (ent instanceof LivingEntity && box.overlaps(ent.getBoundingBox())) {
                    LivingEntity lent = (LivingEntity) ent;
                    EntityDamageByEntityEvent damageEvent = createDamageEvent(fb, ent);
                    Bukkit.getPluginManager().callEvent(damageEvent);
                    if (damageEvent.isCancelled()) continue;

                    lent.damage(damageEvent.getDamage(), fb);
                }
            }
        }
    }

    public EntityDamageByEntityEvent createDamageEvent(FallingBlock fb, Entity ent) {
        Material type = fb.getBlockData().getMaterial();

        double base = type.isOccluding() ? damageBaseOccluding : damageBaseNonOccluding;

        double damage = base + ((type.getBlastResistance() * (base * damageMultiplierBlast)) + (type.getHardness() * (base * damageMultiplierHardness)));

        @SuppressWarnings("removal")
        EntityDamageByEntityEvent damageEvent = new EntityDamageByEntityEvent(fb, ent, EntityDamageEvent.DamageCause.FALLING_BLOCK, damage);
        return damageEvent;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFallingBlockLand(EntityChangeBlockEvent e) {
        if (e.getEntity() instanceof FallingBlock && e.getTo() != Material.AIR) {
            FallingBlock fb = (FallingBlock) e.getEntity();
            PersistentDataContainer cont = fb.getPersistentDataContainer();
            byte[] serialized = cont.get(fallingDataKey, PersistentDataType.BYTE_ARRAY);

            blockChangeSender.queueBlock(e.getBlock());
            
            if (serialized != null) {
                if (isVisualMode(serialized)) {
                    e.setCancelled(true);
                    DebrisBreakEvent dbe = new DebrisBreakEvent(fb);
                    if (dbe.callEvent()) {
                        if (dbe.hasDrops()) {
                            dropItems(fb, dbe.getDrops());
                        }
                        postFall(fb, true);
                    }
                } else if (disabledFallingBlocksPlacement.contains(e.getTo())) {
                    e.setCancelled(true);
                    if (disabledFallingBlockDrops.contains(fb.getBlockData().getMaterial())) {
                        DebrisBreakEvent dbe = new DebrisBreakEvent(fb);
                        if (dbe.callEvent()) {
                            if (dbe.hasDrops()) {
                                dropItems(fb, dbe.getDrops());
                            }
                            postFall(fb, true);
                        }
                    } else {
                        DebrisBreakEvent dbe = new DebrisBreakEvent(fb, deserializeDebrisStacks(serialized));
                        if (dbe.callEvent()) {
                            dropItems(fb, dbe.getDrops());
                            postFall(fb, true);
                        }
                    }
                } else if (new DebrisLandEvent(fb, e).callEvent()) {
                    postFall(fb, false);
                } else {
                    DebrisBreakEvent dbe;
                    if (disabledFallingBlockDrops.contains(fb.getBlockData().getMaterial())) {
                        dbe = new DebrisBreakEvent(fb);
                    } else {
                        dbe = new DebrisBreakEvent(fb, deserializeDebrisStacks(serialized));
                    }
                    if (dbe.callEvent()) {
                        if (dbe.hasDrops()) dropItems(fb, dbe.getDrops());
                        postFall(fb, true);
                    }
                    e.setCancelled(true);
                }
            }
            
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFallingBlockDropItem(EntityDropItemEvent e) {
        if (e.getEntity() instanceof FallingBlock) {
            FallingBlock fb = (FallingBlock) e.getEntity();
            byte[] serialized = fb.getPersistentDataContainer().get(fallingDataKey, PersistentDataType.BYTE_ARRAY);
            if (serialized != null) {
                e.setCancelled(true);
                if (!disabledFallingBlockDrops.contains(fb.getBlockData().getMaterial())) {
                    DebrisBreakEvent dbe;
                    if (isVisualMode(serialized)) {
                        dbe = new DebrisBreakEvent(fb);
                    } else {
                        dbe = new DebrisBreakEvent(fb, deserializeDebrisStacks(serialized));
                    }
                    if (dbe.callEvent()) {
                        if (dbe.hasDrops()) dropItems(fb, dbe.getDrops());
                        postFall(fb, true);
                    }
                }
            }
        }
    }

    public NamespacedKey getFallingKey() {
        return fallingDataKey;
    }

    public boolean isDebris(Entity ent) {
        return ent instanceof FallingBlock && ent.getPersistentDataContainer().has(fallingDataKey, PersistentDataType.BYTE_ARRAY);
    }

    public ItemStack getTool() {
        return tool;
    }

    public boolean isSilkTouch() {
        return silkTouch;
    }

    public boolean isFortune() {
        return fortune > 0;
    }
    
    public void setSilkTouch(boolean silkTouch) {
        if (silkTouch) {
            tool.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 1);
            setFortune(0);
        } else {
            tool.removeEnchantment(Enchantment.SILK_TOUCH);
        }
        this.silkTouch = silkTouch;
    }

    public void setFortune(int level) {
        if (level > 0) {
            tool.addUnsafeEnchantment(api.getFortuneEnchantment(), level);
            setSilkTouch(false);
        } else {
            tool.removeEnchantment(api.getFortuneEnchantment());
        }
        this.fortune = level;
    }
    
    public double getVelocityMultiplier() {
        return velocityMultiplier;
    }
    
    public void setVelocityMultiplier(double velocityMultiplier) {
        this.velocityMultiplier = velocityMultiplier;
    }
    
    public double getVelocityRandom() {
        return velocityRandom;
    }
    
    public void setVelocityRandom(double velocityRandom) {
        this.velocityRandom = velocityRandom;
    }
    
    public int getMaxRayAttempts() {
        return maxRayAttempts;
    }
    
    public void setMaxRayAttempts(int maxRayAttempts) {
        this.maxRayAttempts = maxRayAttempts;
    }
    
    public int getMaxRayDistance() {
        return maxRayDistance;
    }
    
    public void setMaxRayDistance(int maxRayDistance) {
        this.maxRayDistance = maxRayDistance;
    }
    
    public double getBreakChanceNoFree() {
        return breakChanceNoFree;
    }
    
    public void setBreakChanceNoFree(double breakChanceNoFree) {
        this.breakChanceNoFree = breakChanceNoFree;
    }

    public int getMaxDebrisInSimulationDistance() {
        return maxDebrisInSimulationDistance;
    }

    public void setMaxDebrisInSimulationDistance(int maxDebrisInSimulationDistance) {
        this.maxDebrisInSimulationDistance = maxDebrisInSimulationDistance;
    }

    public int getTicksBeforeUpdate() {
        return blockChangeSender.getDelay();
    }

    public void setTicksBeforeUpdate(int ticksBeforeUpdate) {
        blockChangeSender.close();
        blockChangeSender = new BlockChangeSender(kaboom, ticksBeforeUpdate);
    }
    
    public boolean doFallingBlocksHurtEntities() {
        return fallingBlocksHurtEntities;
    }
    
    public void setFallingBlocksHurtEntities(boolean fallingBlocksHurtEntities) {
        this.fallingBlocksHurtEntities = fallingBlocksHurtEntities;
    }
    
    public boolean getFallingBlockBreakAnimation() {
        return fallingBlockBreakAnimation;
    }
    
    public void setFallingBlockBreakAnimation(boolean fallingBlockBreakAnimation) {
        this.fallingBlockBreakAnimation = fallingBlockBreakAnimation;
    }
    
    public boolean isFallingBlockVisualOnlyMode() {
        return fallingBlockVisualOnlyMode;
    }
    
    public void setFallingBlockVisualOnlyMode(boolean fallingBlockVisualOnlyMode) {
        this.fallingBlockVisualOnlyMode = fallingBlockVisualOnlyMode;
    }

    public double getDamageBaseOccluding() {
        return damageBaseOccluding;
    }

    public void setDamageBaseOccluding(double damageBaseOccluding) {
        this.damageBaseOccluding = damageBaseOccluding;
    }

    public double getDamageBaseNonOccluding() {
        return damageBaseNonOccluding;
    }

    public void setDamageBaseNonOccluding(double damageBaseNonOccluding) {
        this.damageBaseNonOccluding = damageBaseNonOccluding;
    }

    public double getDamageMultiplierBlast() {
        return damageMultiplierBlast;
    }

    public void setDamageMultiplierBlast(double damageMultiplierBlast) {
        this.damageMultiplierBlast = damageMultiplierBlast;
    }

    public double getDamageMultiplierHardness() {
        return damageMultiplierHardness;
    }

    public void setDamageMultiplierHardness(double damageMultiplierHardness) {
        this.damageMultiplierHardness = damageMultiplierHardness;
    }
    
    public Set<Material> getDisabledFallingBlocks() {
        return disabledFallingBlocks;
    }
    
    public HashSet<Material> getDisabledFallingBlocksPlacement() {
        return disabledFallingBlocksPlacement;
    }
    
    public HashSet<Material> getDisabledFallingBlockDrops() {
        return disabledFallingBlockDrops;
    }

    @Override
    public void close() {
        blockChangeSender.close();
    }

    public Integer metricsExplodedBlocks() {
        Integer clampedExplodedBlocks = (int) Math.min(Integer.MAX_VALUE, explodedBlocks);
        explodedBlocks = 0;
        return clampedExplodedBlocks;
    }

}
