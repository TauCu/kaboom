package me.taucu.kaboom;

import me.taucu.kaboom.commands.CommandKaBoom;
import me.taucu.kaboom.compatibility.APIAccess;
import me.taucu.kaboom.compatibility.VersionSupport;
import me.taucu.kaboom.hooks.Hooks;
import me.taucu.kaboom.physics.ExplosionHandler;
import me.taucu.kaboom.util.EventUtil;
import me.taucu.kaboom.util.LocationUtil;
import net.md_5.bungee.api.ChatColor;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.AdvancedPie;
import org.bstats.charts.SingleLineChart;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

public class KaBoom extends JavaPlugin implements Listener {
    
    private static final String art = ChatColor.translateAlternateColorCodes('&', "\r\n"
            + "&r#################################\r\n"
            + "&r#&c       _.-^^---....,,-._       &r#\r\n"
            + "&r#&c   _--                   '-_   &r#\r\n"
            + "&r#&c  <                        >)  &r#\r\n"
            + "&r#&c  |        KA BOOM!        |   &r#\r\n"
            + "&r#&c   \\._                   _./   &r#\r\n"
            + "&r#&4      ```--. . , ; .--'''      &r#\r\n"
            + "&r#&4            | |   |            &r#\r\n"
            + "&r#&4         .-=||  | |=-.         &r#\r\n"
            + "&r#&4         `-=#$%&%$#=-'         &r#\r\n"
            + "&r#&4            | ;  :|            &r#\r\n"
            + "&r#&7________.,-#%&$@%#&#~,.________&r#\r\n"
            + "&r#&7              By               &r#\r\n"
            + "&r#&7           TauCubed            &r#");
    
    private static KaBoom instance = null;
    
    public static KaBoom get() {
        return instance;
    }

    private APIAccess api;
    private Hooks hooks = new Hooks(this);
    private ExplosionHandler handler = null;

    private final HashSet<EntityType> noDebrisForEntities = new HashSet<>();
    private boolean blockExplosionDebris = true;

    private final HashMap<EntityType, Float> entityYieldModifiers = new HashMap<>();
    private float blockYieldModifier = -1.0F;

    private final HashSet<EntityType> disabledEntities = new HashSet<>();

    private Metrics metrics = null;

    @Override
    public void onLoad() {
        instance = this;
        // load hooks
        hooks.loadRegisteredHooks();
    }

    @Override
    public void onEnable() {
        String ver = getDescription().getVersion();
        StringBuilder sb = new StringBuilder("#                               #");
        int start = Math.max(0, (sb.length() / 2) - (ver.length() / 2));
        int i = 0;
        for (char c : ver.toCharArray()) {
            if (sb.length() > i) {
                sb.setCharAt(start + i, c);
            } else {
                sb.append(c);
            }
            i++;
        }
        Bukkit.getConsoleSender().sendMessage(art + "\r\n" + sb + "\r\n#################################");

        // setup version support
        VersionSupport.boot(this);
        api = VersionSupport.get();

        // initialize explosion handler
        handler = new ExplosionHandler(this);
        
        // register commands
        final CommandKaBoom cmd = new CommandKaBoom();
        final PluginCommand pcmd = getCommand("kaboom");
        pcmd.setExecutor(cmd);
        pcmd.setTabCompleter(cmd);

        // load the config
        loadConfig();

        // enable hooks
        hooks.enableLoadedHooks();

        // register events
        Bukkit.getPluginManager().registerEvents(handler, this);
        Bukkit.getPluginManager().registerEvents(this, this);
        Runnable enforce = () -> { // ensure that our priorities are always first/last
            EventUtil.enforcePriority(handler, this);
            EventUtil.enforcePriority(this, this);
        };

        enforce.run();
        Bukkit.getScheduler().runTask(this, enforce);

        // try to create metrics
        try {
            this.metrics = new Metrics(this, 14776);
            // for any hook, named block_logging_hooks for legacy support
            metrics.addCustomChart(new AdvancedPie("block_logging_hooks", hooks::metricsEnabledHooks));

            metrics.addCustomChart(new SingleLineChart("exploded_blocks", handler::metricsExplodedBlocks));
        } catch (Exception e) {
            getLogger().log(Level.WARNING, "Error while enabling bStats metrics", e);
        }

        getLogger().info("Enabled KaBoom!");
    }
    
    @Override
    public void onDisable() {
        if (handler != null) {
            handler.close();
        }
    }
    
    /**
     * Loads/Reloads the configuration from file
     */
    public void loadConfig() {
        this.saveDefaultConfig();
        this.reloadConfig();
        if (getConfig().getInt("config version") != getConfig().getDefaults().getInt("config version")) {
            getLogger().warning("Your configuration is outdated. It is advisable to regenerate your configuration.");
        }
        
        final Configuration c = getConfig();
        
        handler.setVelocityMultiplier(c.getDouble("physics.velocity multiplier"));
        handler.setVelocityRandom(c.getDouble("physics.velocity random"));
        
        handler.setMaxRayAttempts(c.getInt("physics.max ray attempts"));
        handler.setMaxRayDistance(c.getInt("physics.max ray distance"));
        handler.setBreakChanceNoFree(c.getDouble("physics.break chance if no free trajectory"));
        handler.setMaxDebrisInSimulationDistance(c.getInt("physics.max debris in simulation distance"));
        handler.setTicksBeforeUpdate(c.getInt("physics.ticks before update"));
        
        handler.setSilkTouch(c.getBoolean("silk touch blocks"));
        if (!handler.isSilkTouch()) {
            handler.setFortune(c.getInt("fortune level"));
        }

        handler.setFallingBlockBreakAnimation(c.getBoolean("physics.play falling break animation"));
        
        handler.setFallingBlockVisualOnlyMode(c.getBoolean("physics.falling blocks are visual only"));

        handler.setFallingBlocksHurtEntities(c.getBoolean("physics.falling blocks hurt entities"));
        handler.setDamageBaseOccluding(c.getDouble("physics.falling block damage.base occluding"));
        handler.setDamageBaseNonOccluding(c.getDouble("physics.falling block damage.base non-occluding"));
        handler.setDamageMultiplierBlast(c.getDouble("physics.falling block damage.blast resistance multiplier"));
        handler.setDamageMultiplierHardness(c.getDouble("physics.falling block damage.material hardness multiplier"));

        handler.getDisabledFallingBlocks().clear();
        for (String s : c.getStringList("physics.disabled falling blocks")) {
            try {
                addOrAddAll(handler.getDisabledFallingBlocks(), s);
            } catch (IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find material by name \"" + s + "\" for \"physics.disabled falling blocks\"");
            }
        }
        
        handler.getDisabledFallingBlocksPlacement().clear();
        for (String s : c.getStringList("physics.disabled falling blocks placement")) {
            try {
                addOrAddAll(handler.getDisabledFallingBlocksPlacement(), s);
            } catch (IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find material by name \"" + s + "\" for \"physics.disabled falling blocks placement\"");
            }
        }
        
        handler.getDisabledFallingBlockDrops().clear();
        for (String s : c.getStringList("physics.disabled falling block drops")) {
            try {
                addOrAddAll(handler.getDisabledFallingBlockDrops(), s);
            } catch (IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find material by name \"" + s + "\" for \"physics.disabled falling block drops\"");
            }
        }

        noDebrisForEntities.clear();
        for (String s : c.getStringList("physics.disable debris for entities")) {
            try {
                noDebrisForEntities.add(EntityType.valueOf(s.toUpperCase()));
            } catch (IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find entity by name \"" + s + "\" for \"physics.disable debris for entities\"");
            }
        }

        blockExplosionDebris = !c.getBoolean("physics.disable block explosion debris");

        entityYieldModifiers.clear();
        ConfigurationSection eym = c.getConfigurationSection("entity yield modifiers");
        for (String k : eym.getKeys(false)) {
            try {
                entityYieldModifiers.put(EntityType.valueOf(k.toUpperCase()), (float) eym.getDouble(k, -1));
            } catch (IllegalArgumentException e) {
                if (e instanceof NumberFormatException) {
                    getLogger().log(Level.WARNING, "The number \"" + eym.getDouble(k) + "\" for \"entity yield modifiers\" is not a valid number");
                } else {
                    getLogger().log(Level.WARNING, "Couldn't find entity by name \"" + k + "\" for \"entity yield modifiers\"");
                }
            }
        }

        blockYieldModifier = (float) c.getDouble("block yield modifier", -1);

        disabledEntities.clear();
        for (String s : c.getStringList("disabled entities")) {
            try {
                disabledEntities.add(EntityType.valueOf(s.toUpperCase()));
            } catch (IllegalArgumentException e) {
                if (c.getDefaults().getStringList("disabled entities").contains(s)) continue;
                getLogger().log(Level.WARNING, "Couldn't find entity by name \"" + s + "\" for \"disabled entities\"");
            }
        }
    }
    
    /**
     * Adds a {@link Material} by name or every {@link Material} if the value is "EVERYTHING"
     * 
     * @param set The {@link Set} to add values to
     * @param value   The value
     * @throws IllegalArgumentException If an invalid argument is used
     */
    private void addOrAddAll(Set<Material> set, String value) throws IllegalArgumentException {
        if (value.equalsIgnoreCase("EVERYTHING")) {
            set.addAll(Arrays.asList(Material.values()));
        } else {
            set.add(Material.valueOf(value.toUpperCase()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockExplode(BlockExplodeEvent e) {
        if (!api.isRealExplosion(e)) return;
        float yield;
        if (blockYieldModifier < 0 || blockYieldModifier > 1) {
            yield = e.getYield();
        } else {
            e.setYield(blockYieldModifier);
            yield = blockYieldModifier;
        }
        handler.handleExplosion(
                LocationUtil.toCenterLocation(e.getBlock().getLocation()),
                e.blockList(),
                yield,
                blockExplosionDebris,
                e.getBlock()
        );
        e.setYield(0);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent e) {
        if (!api.isRealExplosion(e)) return;
        EntityType entType = e.getEntityType();
        if (disabledEntities.contains(entType)) return;
        Float yield = entityYieldModifiers.get(entType);
        if (yield == null || yield < 0 || yield > 1) {
            yield = e.getYield();
        }
        handler.handleExplosion(
                e.getLocation().clone().add(0, 0.5, 0),
                e.blockList(),
                yield,
                !noDebrisForEntities.contains(entType),
                e.getEntity()
        );
        e.setYield(0);
    }

    /**
     * Get the Hook api
     *
     * @return the instance of the {@link Hooks} api
     */
    public Hooks getHooks() {
        return hooks;
    }

    /**
     * Get the Explosion Handler
     *
     * @return the instance of the {@link ExplosionHandler}
     */
    public ExplosionHandler getHandler() {
        return handler;
    }

    /**
     * Get the {@link HashSet} of entities that will not create debris
     * @return the {@link HashSet} of entities
     */
    public HashSet<EntityType> getDebrislessEntities() {
        return noDebrisForEntities;
    }

    /**
     * Should Block Explosions create debris?
     * @return true for debris, false otherwise
     */
    public boolean isBlockExplosionDebris() {
        return blockExplosionDebris;
    }

    /**
     * Set if Block Explosions should create debris
     * @param blockExplosionDebris true for debris, false otherwise
     */
    public void setBlockExplosionDebris(boolean blockExplosionDebris) {
        this.blockExplosionDebris = blockExplosionDebris;
    }

    /**
     * used to modify the yield of entity explosions
     * @return The entity yield modifiers map
     */
    public HashMap<EntityType, Float> getEntityYieldModifiers() {
        return entityYieldModifiers;
    }

    /**
     * Used to modify the yield of all block explosions
     * @return The yield modifier
     */
    public float getBlockYieldModifier() {
        return blockYieldModifier;
    }

    /**
     * Used to modify the yield of all block explosions
     * @param blockYieldModifier The yield modifier (-1 to disable modification)
     */
    public void setBlockYieldModifier(float blockYieldModifier) {
        this.blockYieldModifier = blockYieldModifier;
    }
    
}
