package me.taucu.kaboom.util;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class RayBouncer {

    private RayBouncer() {}
    
    /**
     * Casts a Block-intersecting ray that ignores {@link Block#isPassable}
     * blocks, bouncing one it hits an impassible block.
     * 
     * @param toIgnore    {@link HashSet} of blocks for the ray to ignore
     * @param loc         {@link Location} start of the ray
     * @param vel         {@link Vector} direction of the ray (retains magnitude)
     * @param maxBounces  maximum bounces of this operation
     * @param bounceRange the maximum range of each bounce
     * @return {@link RayBounceResult}
     */
    public static RayBounceResult rayBounce(Set<Block> toIgnore, Location loc, Vector vel, int maxBounces, int bounceRange) {
        BlockIterator it = new BlockIterator(loc.getWorld(), loc.toVector(), vel, 0, bounceRange);
        Block prevBlock = loc.getBlock();
        Block lastFree = prevBlock;
        int bounces = 0;
        boolean foundFree = true;
        
        while (it.hasNext()) {
            Block b = it.next();
            // loc.getWorld().spawnParticle(Particle.REDSTONE, b.getLocation(), 1, new Particle.DustOptions(Color.RED, 1));
            
            if (!b.isPassable() && !toIgnore.contains(b)) {
                if (bounces < maxBounces) {
                    bounce(b.getFace(prevBlock).getDirection(), vel);
                    bounces++;
                    it = new BlockIterator(loc.getWorld(), LocationUtil.toCenterLocation(prevBlock.getLocation()).toVector(), vel, 0, bounceRange);
                } else {
                    foundFree = false;
                    break;
                }
            } else if (!prevBlock.isPassable() && !toIgnore.contains(prevBlock)) {
                lastFree = b;
            }
            prevBlock = b;
        }
        
        return new RayBounceResult(lastFree.getLocation(), vel, foundFree);
    }
    
    /**
     * Bounces a {@link Vector} off of a surface
     * 
     * @param planeNormal Normalized {@link Vector} representing the surface to bounce off of
     * @param vel         The {@link Vector} to bounce
     * @return bounced input {@link Vector} (retains magnitude)
     */
    public static Vector bounce(Vector planeNormal, Vector vel) {
        return vel.subtract(planeNormal.multiply(vel.dot(planeNormal) * 2.0D));
    }

    public static class RayBounceResult {
        public final Location location;
        public final Vector velocity;
        public final boolean foundFreeVector;

        public RayBounceResult(Location location, Vector velocity, boolean foundFreeVector) {
            this.location = location;
            this.velocity = velocity;
            this.foundFreeVector = foundFreeVector;
        }
    }
    
}
