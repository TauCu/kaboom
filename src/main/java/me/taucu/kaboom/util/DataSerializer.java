package me.taucu.kaboom.util;

import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class DataSerializer {

    public static final int DATA_VERSION = 0;
    
    private DataSerializer() {}

    public static byte[] serializeDebrisData(Collection<ItemStack> stacks) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (BukkitObjectOutputStream os = new BukkitObjectOutputStream(bos)) {
            os.writeInt(DATA_VERSION); // data ver
            os.writeInt(stacks.size()); // length
            for (ItemStack is : stacks) {
                os.writeObject(is);
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not serialize ItemStacks", e);
        }
        return bos.toByteArray();
    }

    public static ArrayList<ItemStack> deserializeDebrisData(byte[] data, int max) {
        try (BukkitObjectInputStream os = new BukkitObjectInputStream(new ByteArrayInputStream(data))) {
            int ver = os.readInt(); // data ver
            if (ver > DATA_VERSION) {
                throw new IOException("Data was created by a newer version of KaBoom");
            }
            int len = Math.min(max, os.readInt()); // length
            ArrayList<ItemStack> stacks = new ArrayList<>(len);
            for (int i = 0; i < len; i++) {
                stacks.add((ItemStack) os.readObject());
            }
            return stacks;
        } catch (Exception e) {
            throw new RuntimeException("Could not deserialize ItemStacks", e);
        }
    }
    
}
