package me.taucu.kaboom.util;

import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.plugin.PluginManager;

public interface CallableEvent {

    PluginManager PLUGIN_MANAGER = Bukkit.getPluginManager();

    default boolean callEvent() {
        PLUGIN_MANAGER.callEvent((Event) this);
        if (this instanceof Cancellable) {
            return !((Cancellable) this).isCancelled();
        }
        return true;
    }

}
