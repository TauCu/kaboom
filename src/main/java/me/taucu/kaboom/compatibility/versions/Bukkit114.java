package me.taucu.kaboom.compatibility.versions;

import me.taucu.kaboom.compatibility.APIAccess;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.enchantments.Enchantment;

import java.util.Objects;

public class Bukkit114 implements APIAccess {

    private Particle legacyBlockDust;
    private Enchantment fortuneEnchantment;

    public boolean shouldBlow(Material type) {
        switch (type) {
            case TNT:
            case AIR:
            case CAVE_AIR:
            case VOID_AIR:
                return true;
            default:
                return false;
        }
    }

    public boolean isAir(Material type) {
        switch (type) {
            case AIR:
            case CAVE_AIR:
            case VOID_AIR:
                return true;
            default:
                return false;
        }
    }

    @Override
    public Particle getBlockDustParticle() {
        return legacyBlockDust == null ? (legacyBlockDust = Particle.valueOf("BLOCK_DUST")) : legacyBlockDust;
    }

    @Override
    public Enchantment getFortuneEnchantment() {
        return fortuneEnchantment == null ? (fortuneEnchantment = Objects.requireNonNull(Enchantment.getByName("LOOT_BONUS_BLOCKS"), "enchantment")) : fortuneEnchantment;
    }

}
