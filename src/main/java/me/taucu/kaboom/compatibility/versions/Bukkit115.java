package me.taucu.kaboom.compatibility.versions;

import me.taucu.kaboom.compatibility.APIAccess;
import org.bukkit.Material;
import org.bukkit.World;

public class Bukkit115 extends Bukkit114 implements APIAccess {

    @Override
    public boolean shouldBlow(Material type) {
        return type == Material.TNT || type.isAir();
    }

    @Override
    public boolean isAir(Material type) {
        return type.isAir();
    }

    @Override
    public int getViewDistance(World world) {
        return world.getViewDistance();
    }

}
