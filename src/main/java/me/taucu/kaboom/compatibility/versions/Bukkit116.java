package me.taucu.kaboom.compatibility.versions;

import me.taucu.kaboom.compatibility.APIAccess;
import org.bukkit.Location;
import org.bukkit.SoundCategory;
import org.bukkit.SoundGroup;
import org.bukkit.block.data.BlockData;

public class Bukkit116 extends Bukkit115 implements APIAccess {

    public void playBreakSound(Location loc, BlockData data) {
        SoundGroup group = data.getSoundGroup();
        loc.getWorld().playSound(loc, group.getBreakSound(), SoundCategory.BLOCKS, group.getVolume() * 1.5F, group.getPitch());
    }

}
