package me.taucu.kaboom.compatibility.versions;

import me.taucu.kaboom.compatibility.APIAccess;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public interface IPaper116 extends APIAccess {

    @Override
    default void breakBlock(Block block, ItemStack tool) {
        block.breakNaturally(tool, false);
    }

}
