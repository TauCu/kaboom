package me.taucu.kaboom.compatibility.versions;

import org.bukkit.ExplosionResult;
import org.bukkit.Particle;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class Bukkit121 extends Bukkit116 {

    @Override
    public boolean isRealExplosion(BlockExplodeEvent e) {
        return isRealExplosionResult(e.getExplosionResult());
    }

    @Override
    public boolean isRealExplosion(EntityExplodeEvent e) {
        return isRealExplosionResult(e.getExplosionResult());
    }

    public static boolean isRealExplosionResult(ExplosionResult r) {
        return r == ExplosionResult.DESTROY || r == ExplosionResult.DESTROY_WITH_DECAY;
    }

    @Override
    public Particle getBlockDustParticle() {
        return Particle.BLOCK;
    }

    @Override
    public Enchantment getFortuneEnchantment() {
        return Enchantment.FORTUNE;
    }

}
