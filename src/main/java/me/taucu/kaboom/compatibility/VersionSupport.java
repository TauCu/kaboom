package me.taucu.kaboom.compatibility;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.compatibility.versions.*;
import org.bukkit.Bukkit;

import java.util.logging.Level;

public class VersionSupport {

    private static APIAccess access;

    public static APIAccess get() {
        return access;
    }

    public static void boot(KaBoom kaboom) {
        String mcVer = getMinecraftVersion();
        boolean isPaper = isPaper();

        int major = 0, minor = 0;
        try {
            String[] vers = mcVer.replaceAll("[^.\\d]", "").split("\\.");
            major = Integer.parseInt(vers[0]);
            minor = Integer.parseInt(vers[1]);
        } catch (NumberFormatException e) {
            kaboom.getLogger().log(Level.SEVERE, "Could not format server version. Defaulting to lowest support for: " + mcVer, e);
        }

        if (major > 0 && minor >= 21) {
            access = isPaper ? new Paper121() : new Bukkit121();
            kaboom.getLogger().info("Loaded support for " + (isPaper ? "Paper " : "Bukkit ") + "1.21+");
        } else if (major > 0 && minor >= 16) {
            access = isPaper ? new Paper116() : new Bukkit116();
            kaboom.getLogger().info("Loaded support for " + (isPaper ? "Paper " : "Bukkit ") + "1.16-1.20");
        } else if (major > 0 && minor == 15) {
            access = new Bukkit115();
            kaboom.getLogger().info("Loaded support for Bukkit 1.15");
        } else {
            access = new Bukkit114();
            kaboom.getLogger().info("Loaded support for Bukkit 1.14");
        }
    }

    public static boolean classExists(String clazz) {
        return getClassOrNull(clazz) != null;
    }

    public static Class<?> getClassOrNull(String clazz) {
        try {
            return Class.forName(clazz, true, KaBoom.class.getClassLoader());
        } catch (Exception ignored) {}
        return null;
    }

    public static String getMinecraftVersion() {
        return Bukkit.getBukkitVersion().split("-")[0];
    }

    public static boolean isPaper() {
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        return cl.getResource("io/papermc/paper") != null || cl.getResource("io/papermc/paperclip") != null
                || cl.getResource("com/destroystokyo/paper") != null || cl.getResource("com/destroystokyo/paperclip") != null;
    }

}
