package me.taucu.kaboom.compatibility;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

public interface APIAccess {

    boolean shouldBlow(Material type);

    boolean isAir(Material type);

    Particle getBlockDustParticle();

    Enchantment getFortuneEnchantment();

    default void breakBlock(Block block, ItemStack tool) {
        block.breakNaturally(tool);
    }

    default void playBreakSound(Location loc, BlockData data) {}

    default int getViewDistance(World world) {
        return Bukkit.getViewDistance();
    }

    default boolean isRealExplosion(BlockExplodeEvent e) {
        return true;
    }

    default boolean isRealExplosion(EntityExplodeEvent e) {
        return true;
    }

}
