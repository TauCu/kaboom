package me.taucu.kaboom.events;

import org.bukkit.entity.FallingBlock;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityChangeBlockEvent;

/**
 * Called when debris land on the ground
 * @see DebrisBreakEvent
 */
public class DebrisLandEvent extends DebrisEvent implements Cancellable {
    
    private static final HandlerList handlers = new HandlerList();

    private final EntityChangeBlockEvent ecbe;
    
    private boolean cancel = false;
    
    public DebrisLandEvent(FallingBlock debris, EntityChangeBlockEvent ecbe) {
        super(debris);
        this.ecbe = ecbe;
    }

    /**
     * @return the original EntityChangeBlockEvent that the debris caused
     */
    public EntityChangeBlockEvent getEntityChangeBlockEvent() {
        return ecbe;
    }
    
    @Override
    public boolean isCancelled() {
        return cancel;
    }

    /**
     * Cancelling this event will cause no block to be placed, no particles, no sounds, no items will be dropped and the debris will disappear.
     * @param cancel if the event should be cancelled or not
     * @see DebrisLandEvent#getEntityChangeBlockEvent()
     */
    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}
