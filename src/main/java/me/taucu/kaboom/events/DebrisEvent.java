package me.taucu.kaboom.events;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.physics.ExplosionHandler;
import me.taucu.kaboom.util.CallableEvent;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.entity.EntityEvent;
import org.bukkit.persistence.PersistentDataType;

public abstract class DebrisEvent extends EntityEvent implements CallableEvent {

    public DebrisEvent(FallingBlock debris) {
        super(debris);
    }

    /**
     * Gets the debris causing this event
     * @return the FallingBlock debris
     * @apiNote same as {@link EntityEvent#getEntity()} but casts to the correct type
     */
    public FallingBlock getDebris() {
        return (FallingBlock) entity;
    }

    /**
     * Checks if the Debris was created in visual mode<br>
     * @return true if it was in visual mode, false otherwise
     */
    public boolean isVisualMode() {
        return ExplosionHandler.isVisualMode(
                getDebris().getPersistentDataContainer().get(
                        KaBoom.get().getHandler().getFallingKey(), PersistentDataType.BYTE_ARRAY
                )
        );
    }

}
