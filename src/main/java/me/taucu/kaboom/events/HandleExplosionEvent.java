package me.taucu.kaboom.events;

import me.taucu.kaboom.util.CallableEvent;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.world.WorldEvent;

import java.util.List;

public class HandleExplosionEvent extends WorldEvent implements Cancellable, CallableEvent {

    private static final HandlerList handlers = new HandlerList();

    private boolean cancelled = false;

    private Location center;
    private List<Block> toBlow;
    private float yield;
    private boolean createDebris;
    private Object source;

    public HandleExplosionEvent(World world, Location center, List<Block> toBlow, float yield, boolean createDebris, Object source) {
        super(world);
        this.center = center;
        this.toBlow = toBlow;
        this.yield = yield;
        this.createDebris = createDebris;
        this.source = source;
    }

    public Location getCenter() {
        return center;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public List<Block> getToBlow() {
        return toBlow;
    }

    public void setToBlow(List<Block> toBlow) {
        this.toBlow = toBlow;
    }

    public float getYield() {
        return yield;
    }

    public void setYield(float yield) {
        this.yield = yield;
    }

    public boolean createsDebris() {
        return createDebris;
    }

    public void setCreatesDebris(boolean createDebris) {
        this.createDebris = createDebris;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
