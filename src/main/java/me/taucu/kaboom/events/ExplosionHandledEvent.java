package me.taucu.kaboom.events;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.physics.ExplosionHandler;
import me.taucu.kaboom.util.CallableEvent;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.HandlerList;
import org.bukkit.event.world.WorldEvent;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.List;

public class ExplosionHandledEvent extends WorldEvent implements CallableEvent {
    private static final HandlerList handlers = new HandlerList();

    private final ExplosionHandler handler;

    private final Location center;

    private final HashSet<Block> allBlocks;
    private final List<Block> toBreak;
    private final List<Block> toBlow;

    private final List<FallingBlock> debris;

    private final float yield;
    private final Object source;

    public ExplosionHandledEvent(World world, ExplosionHandler handler, Location center, HashSet<Block> allBlocks, List<Block> toBreak, List<Block> toBlow, List<FallingBlock> debris, float yield, Object source) {
        super(world);
        this.handler = handler;

        this.center = center;

        this.allBlocks = allBlocks;
        this.toBreak = toBreak;
        this.toBlow = toBlow;

        this.debris = debris;

        this.yield = yield;
        this.source = source;
    }

    public ExplosionHandledEvent(World world, ExplosionHandler handler, Location center, HashSet<Block> allBlocks, List<Block> toBreak, List<Block> toBlow, List<FallingBlock> debris) {
        this(world, handler, center, allBlocks, toBreak, toBlow, debris, 1.0F, null);
    }

    /**
     * @return the {@link ExplosionHandler} calling this event
     */
    public ExplosionHandler getHandler() {
        return handler;
    }

    /**
     * @return the center of the explosion
     */
    public Location getCenter() {
        return center;
    }

    /**
     * The {@link HashSet} of all blocks that will be exploded, broken or have been turned into debris<br>
     * Do not modify this unless you know the side effects
     * @return the {@link HashSet} of blocks
     */
    public HashSet<Block> getAllBlocks() {
        return allBlocks;
    }

    /**
     * The {@link List} of Debris created by the explosion<br>
     * Removing debris from this list does not remove them from the world. to achieve that use {@link #removeDebris(FallingBlock)}
     *
     * @return the {@link List} of debris
     */
    public List<FallingBlock> getDebris() {
        return debris;
    }

    /**
     * Creates debris for a specified block, sets the block to air if successful<br>
     * In the event that breakChanceNoFree rolls to break this method will return null.
     * If you wish this block to be broken add it to {@link #addToBreak(Block) explode normally list}
     * <br><br>
     * It is customary to add the debris (if not null) to the {@link #getDebris() debris list} list for further handling
     * @param block the block to create debris for
     * @return the {@link FallingBlock} debris if successful or null if no free trajectory is found & {@link ExplosionHandler#getBreakChanceNoFree() breakChanceNoFree} rolls to break
     * @see #createDebris(Block, double) createDebris(Block, double) for more info
     */
    public FallingBlock createDebris(Block block) {
        return createDebris(block, handler.getBreakChanceNoFree());
    }

    /**
     * Creates debris for a specified block, sets the block to air if successful<br>
     * In the event that breakChanceNoFree rolls to break this method will return null.
     * If you wish this block to be broken add it to {@link #addToBreak(Block)}
     * <br><br>
     * It is customary to add the debris (if not null) to the {@link #getDebris()} list for further handling
     * @param block the block to create debris for
     * @param yield The chance (0.0-1.0) that we should drop an item from the debris
     * @param breakChanceNoFree the chance (0.0-100.0) this method has to return null if no free trajectory is found
     * @return the {@link FallingBlock} debris if successful or null if not
     * @see ExplosionHandler#createDebris(Vector, Block, Material, HashSet, float, double)
     */
    public FallingBlock createDebris(Block block, float yield, double breakChanceNoFree) {
        return handler.createDebris(center.toVector(), block, block.getType(), allBlocks, yield, breakChanceNoFree);
    }

    /**
     * Same as {@link #createDebris(Block, float, double)} but uses the yield of this event instead
     * @param block the block to create debris for
     * @param breakChanceNoFree the chance (0-100) this method has to return null if no free trajectory is found
     * @return the {@link FallingBlock} debris if successful or null if not
     */
    public FallingBlock createDebris(Block block, double breakChanceNoFree) {
        return handler.createDebris(center.toVector(), block, block.getType(), allBlocks, yield, breakChanceNoFree);
    }

    /**
     * Removes debris from both this event and the world
     * @param debris the debris to remove
     */
    public void removeDebris(FallingBlock debris) {
        this.debris.remove(debris);
        debris.remove();
    }

    /**
     * The {@link List} of blocks that will be broken. Applying enabled effects, such as SilkTouch<br>
     * If you wish to add a block please use {@link #addToBreak(Block)}<br>
     * This list does not naturally include AIR or TNT blocks for that see {@link #getToBlow()}
     * @return the {@link List} of blocks
     */
    public List<Block> getToBreak() {
        return toBreak;
    }

    /**
     * Break a block normally without creating debris after this event has run its course<br>
     * Applying any outstanding effects such as the {@link ExplosionHandler handlers} SilkTouch<br>
     * @param block the block to handle normally
     * @see #getToBlow()
     */
    public void addToBreak(Block block) {
        allBlocks.add(block);
        toBreak.add(block);
    }

    /**
     * The {@link List} of blocks that will be blown up without dropping anything except container loot etc.<br>
     * Normally this list only includes TNT and AIR (somehow explosions can break air)<br>
     * If the yield is below 1.0 this list may also contain random blocks to be destroyed with no drops.
     * You may add or remove blocks from this list
     * @return the {@link List} of blocks
     */
    public List<Block> getToBlow() {
        return toBlow;
    }

    /**
     * The chance (0.0-1.0) that an item should be dropped
     * @apiNote not currently possible to modify here, recommended method would be through {@link KaBoom#getEntityYieldModifiers()} or by {@link org.bukkit.event.entity.EntityExplodeEvent#setYield(float)}
     * @return 0 to 1.0
     */
    public float getYield() {
        return yield;
    }

    /**
     * Get the source of the explosion. Could be a block or an entity, perhaps something else.
     * @return the source of the explosion, or null
     */
    public Object getSource() {
        return source;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
