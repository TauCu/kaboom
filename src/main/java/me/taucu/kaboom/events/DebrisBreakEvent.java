package me.taucu.kaboom.events;

import org.bukkit.entity.FallingBlock;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Called when Debris fails to land and instead breaks
 * @see DebrisLandEvent
 */
public class DebrisBreakEvent extends DebrisEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private boolean cancel = false;

    private ArrayList<ItemStack> drops;

    public DebrisBreakEvent(FallingBlock debris, ArrayList<ItemStack> drops) {
        this(debris);
        this.drops = drops;
    }

    public DebrisBreakEvent(FallingBlock debris) {
        super(debris);
    }

    /**
     * Checks if this debris will drop anything
     * @return true if items will be dropped false otherwise
     */
    public boolean hasDrops() {
        return drops != null && !drops.isEmpty();
    }

    /**
     * Gets the mutable list of items the debris will drop
     * @return The mutable list of item(s) the debris will drop, or an empty list of the debris drops nothing
     * @apiNote It is preferable to use hasDrops to check if the debris will drop anything as this method instantiates a new ArrayList of the drops are null
     */
    public ArrayList<ItemStack> getDrops() {
        if (drops == null) {
            drops = new ArrayList<>();
        }
        return drops;
    }

    /**
     * Sets what items the debris will drop.
     * @param drops the list of items this debris will drop
     */
    public void setDrops(ArrayList<ItemStack> drops) {
        this.drops = drops;
    }

    /**
     * Sets what items the debris will drop.
     * @param drops the collection of items this debris will drop
     */
    public void setDrops(Collection<ItemStack> drops) {
        this.drops = new ArrayList<>(drops);
    }

    @Override
    public boolean isCancelled() {
        return cancel;
    }

    /**
     * Cancelling this event will cause no items to be dropped, no particles, no sounds and the debris will disappear.
     * @param cancel if the event should be cancelled
     * @see DebrisBreakEvent#setDrops(ArrayList)
     */
    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
