package me.taucu.kaboom.hooks;

import me.taucu.kaboom.events.DebrisBreakEvent;
import me.taucu.kaboom.events.DebrisLandEvent;
import net.coreprotect.CoreProtect;
import net.coreprotect.CoreProtectAPI;
import net.coreprotect.listener.player.PlayerDropItemListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.inventory.ItemStack;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.WrongMethodTypeException;
import java.util.logging.Level;

public class CoreProtectHook extends Hook implements Listener {

    private CoreProtectAPI api;
    private final MethodHandle emptyLogItemDropHandle = MethodHandles.empty(MethodType.methodType(void.class, Location.class, String.class, ItemStack.class));
    private MethodHandle logItemDropHandle = null;

    @Override
    public void onEnable() {
        kaboom.getLogger().info("Enabling CoreProtect hook...");
        this.api = CoreProtect.getInstance().getAPI();
        if (api == null || !api.isEnabled()) {
            throw new IllegalStateException("CoreProtect API is not enabled! Cannot enable hook; Ensure API is enabled in CoreProtect config.");
        }

        try {
            logItemDropHandle = MethodHandles.privateLookupIn(PlayerDropItemListener.class, MethodHandles.lookup())
                    .findStatic(PlayerDropItemListener.class, "playerDropItem", emptyLogItemDropHandle.type());
        } catch (NoSuchMethodException | IllegalAccessException e) {
            kaboom.getLogger().log(Level.SEVERE, "Couldn't create MethodHandle for Item drop logging; Debris item drops will not be logged", e);
            logItemDropHandle = emptyLogItemDropHandle;
        }

        Bukkit.getPluginManager().registerEvents(this, this.kaboom);
        kaboom.getLogger().info("CoreProtect hook enabled.");
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
        api = null;
        logItemDropHandle = null;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDebrisLand(DebrisLandEvent e) {
        final EntityChangeBlockEvent ecbe = e.getEntityChangeBlockEvent();
        api.logPlacement("#debris", ecbe.getBlock().getLocation(), ecbe.getTo(), ecbe.getBlockData());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDebrisBreak(DebrisBreakEvent e) {
        try {
            Location loc = e.getDebris().getLocation();
            for (ItemStack item : e.getDrops()) {
                logItemDropHandle.invoke(loc, "#debris", item);
            }
        } catch (WrongMethodTypeException ex) {
            logItemDropHandle = emptyLogItemDropHandle;
            kaboom.getLogger().log(Level.SEVERE, "Error while logging debris item drops; item logging now disabled.", ex);
        } catch (Throwable yeet) {
            kaboom.getLogger().log(Level.SEVERE, "Error while logging debris item drops:", yeet);
        }
    }

}
