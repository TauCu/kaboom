package me.taucu.kaboom.hooks;

import me.botsko.prism.Prism;
import me.botsko.prism.actionlibs.ActionFactory;
import me.botsko.prism.actionlibs.ActionTypeImpl;
import me.botsko.prism.actionlibs.RecordingQueue;
import me.botsko.prism.actions.BlockAction;
import me.botsko.prism.events.PrismLoadedEvent;
import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.events.DebrisLandEvent;
import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;

import java.util.logging.Level;

public class PrismHook extends Hook implements Listener {

    private PrismLoadedListener prismLoadedListener = null;

    @Override
    public void onEnable() {
        kaboom.getLogger().info("Enabling Prism hook...");
        if (Prism.getActionRegistry() == null) {
            prismLoadedListener = new PrismLoadedListener(kaboom, this);
            Bukkit.getPluginManager().registerEvents(prismLoadedListener, kaboom);
        } else {
            registerAction();
        }
    }

    protected void registerAction() {
        try {
            Prism.getActionRegistry().registerCustomAction(kaboom, new ActionTypeImpl("debris--land", true, true, true, BlockAction.class, "deposited"));
            Bukkit.getPluginManager().registerEvents(this, kaboom);
            kaboom.getLogger().info("Prism hook enabled.");
            prismLoadedListener = null;
        } catch (Exception ex) {
            kaboom.getLogger().log(Level.SEVERE, "unable to register debris--land ActionType, hook will not function!", ex);
            disable();
        }
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
        if (prismLoadedListener != null) {
            HandlerList.unregisterAll(prismLoadedListener);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDebrisLand(DebrisLandEvent e) {
        final EntityChangeBlockEvent ecbe = e.getEntityChangeBlockEvent();
        final BlockState state = ecbe.getBlock().getState();
        state.setBlockData(ecbe.getBlockData());
        RecordingQueue.addToQueue(ActionFactory.createBlock("debris--land", state, "debris"));
    }

    public static class PrismLoadedListener implements Listener {

        private final KaBoom kaboom;
        private final PrismHook hook;

        public PrismLoadedListener(KaBoom kaboom, PrismHook hook) {
            this.kaboom = kaboom;
            this.hook = hook;
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onPrismLoaded(PrismLoadedEvent e) {
            hook.registerAction();
            HandlerList.unregisterAll(this);
        }

    }

}
