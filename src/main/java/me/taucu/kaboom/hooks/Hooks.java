package me.taucu.kaboom.hooks;

import me.taucu.kaboom.KaBoom;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;

public class Hooks {

    private static final HashMap<String, RegisteredHook> registeredHookNames = new HashMap<>();
    private static final IdentityHashMap<Class<? extends Hook>, RegisteredHook> registeredHookClasses = new IdentityHashMap<>();

    private final KaBoom kaboom;
    private boolean enabled;

    public Hooks(KaBoom kaboom) {
        this.kaboom = kaboom;
        registerHook("CoreProtect", CoreProtectHook.class, "CoreProtect");
        registerHook("LogBlock", LogBlockHook.class, "LogBlock");
        registerHook("Prism", PrismHook.class, "Prism");
        registerHook("WorldGuard", WorldGuardHook.class, "WorldGuard");
    }

    public static void registerHook(String name, Class<? extends Hook> hook, String pluginDependName) {
        registerHook(name, hook, kaboom -> {
            if (Bukkit.getPluginManager().getPlugin(pluginDependName) != null) {
                try {
                    return hook.getConstructor().newInstance();
                } catch (ReflectiveOperationException e) {
                    kaboom.getLogger().log(Level.SEVERE, "Failed to construct hook", e);
                }
            } else {
                kaboom.getLogger().warning("Cannot load " + name + " hook. " + pluginDependName + " plugin not found.");
            }
            return null;
        });
    }

    public static void registerHook(String name, Class<? extends Hook> hook, Function<KaBoom, Hook> loadConstructor) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(hook);
        if (!registeredHookNames.containsKey(name)) {
            RegisteredHook rh = new RegisteredHook(name, hook, loadConstructor);
            registeredHookClasses.put(hook, rh);
            registeredHookNames.put(name.toLowerCase(), rh);
        }
    }

    public boolean isHookEnabled(String hookName) {
        ConfigurationSection hooksSection = KaBoom.get().getConfig().getConfigurationSection("hooks");
        for (String k : hooksSection.getKeys(false)) {
            if (k.equalsIgnoreCase(hookName + " hook") && hooksSection.getBoolean(k)) {
                return true;
            }
        }
        return false;
    }

    public void loadRegisteredHooks() {
        for (RegisteredHook rh : registeredHookNames.values()) {
            Hook hook;
            try {
                hook = rh.loadConstructor.apply(kaboom);
            } catch (Throwable t) {
                if (t instanceof ThreadDeath) throw t;
                kaboom.getLogger().log(Level.SEVERE, "Error while loading hook: " + rh.name, t);
                return;
            }
            if (hook != null) {
                loadHook(hook);
            }
        }
    }

    public boolean loadHook(Hook h) {
        RegisteredHook rh = registeredHookClasses.get(h.getClass());
        if (rh == null || rh.loadedHook != null) {
            return false;
        } else {
            try {
                rh.loadedHook = h;
                h.onLoad();
                if (enabled) enableHook(h);
                return true;
            } catch (Throwable t) {
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath) t;
                }
                kaboom.getLogger().log(Level.SEVERE, "Error while loading hook \"" + h.getClass().getSimpleName() + "\"", t);
                return false;
            }
        }
    }

    public void enableLoadedHooks() {
        for (RegisteredHook rh : registeredHookClasses.values()) {
            if (rh.loadedHook != null && isHookEnabled(rh.name)) enableHook(rh.loadedHook);
        }
    }

    public boolean enableHook(Hook h) {
        RegisteredHook rh = registeredHookClasses.get(h.getClass());
        if (rh != null && rh.loadedHook == h) {
            try {
                return h.enable();
            } catch (Throwable t) {
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath) t;
                }
                kaboom.getLogger().log(Level.SEVERE, "Error while enabling hook \"" + h.getClass().getSimpleName() + "\"", t);
                disableHook(h);
                return false;
            }
        }
        return false;
    }

    public void disableAllHooks() {
        for (RegisteredHook rh : getRegisteredHooks()) {
            if (rh.loadedHook != null) disableHook(rh.loadedHook);
        }
    }

    public Collection<RegisteredHook> getRegisteredHooks() {
        return registeredHookClasses.values();
    }

    public boolean disableHook(Hook h) {
        try {
            h.disable();
            return true;
        } catch (Throwable t) {
            kaboom.getLogger().log(Level.SEVERE, "Error while disabling hook \"" + h.getClass().getSimpleName() + "\"", t);
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Hook> T getHook(Class<T> clazz) {
        return (T) (registeredHookClasses.get(clazz) != null ? registeredHookClasses.get(clazz).loadedHook : null);
    }

    public boolean isHookEnabled(Class<? extends Hook> clazz) {
        Hook h = getHook(clazz);
        return h != null && h.isEnabled();
    }

    public Map<String, Integer> metricsEnabledHooks() {
        HashMap<String, Integer> map = new HashMap<>();
        if (isHookEnabled(CoreProtectHook.class)) map.put("CoreProtect", 1);
        if (isHookEnabled(LogBlockHook.class)) map.put("LogBlock", 1);
        if (isHookEnabled(PrismHook.class)) map.put("Prism", 1);
        if (isHookEnabled(WorldGuardHook.class)) map.put("WorldGuard", 1);

        if (map.isEmpty()) map.put("None", 1);

        return map;
    }

    private static class RegisteredHook {
        public final String name;
        public final Class<? extends Hook> hook;
        public final Function<KaBoom, Hook> loadConstructor;
        protected Hook loadedHook;
        public Hook getLoadedHook() {
            return loadedHook;
        }

        private RegisteredHook(String name, Class<? extends Hook> hook, Function<KaBoom, Hook> loadConstructor) {
            this.name = name;
            this.hook = hook;
            this.loadConstructor = loadConstructor;
        }
    }

}
