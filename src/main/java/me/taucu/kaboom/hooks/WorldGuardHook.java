package me.taucu.kaboom.hooks;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import me.taucu.kaboom.events.DebrisBreakEvent;
import me.taucu.kaboom.events.DebrisLandEvent;
import me.taucu.kaboom.events.HandleExplosionEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class WorldGuardHook extends Hook implements Listener {

    public static final String FLAG_CREATE_DEBRIS = "kaboom-create-debris";
    public static final String FLAG_DEBRIS_LAND = "kaboom-debris-land";
    public static final String FLAG_DEBRIS_DROP = "kaboom-debris-drop";
    public static final String FLAG_DEBRIS_DAMAGE = "kaboom-debris-damage";
    private StateFlag createDebrisFlag;
    private StateFlag debrisLandFlag;
    private StateFlag debrisDropFlag;
    private StateFlag debrisDamageFlag;

    @Override
    public void onEnable() {
        kaboom.getLogger().info("Enabling WorldGuard hook...");
        if (!Bukkit.getPluginManager().isPluginEnabled("WorldGuard")) throw new IllegalStateException("WorldGuard is loaded but not enabled");
        Bukkit.getPluginManager().registerEvents(this, kaboom);
    }

    @Override
    public void onDisable() {}

    @Override
    public void onLoad() {
        createDebrisFlag = registerStateFlag(FLAG_CREATE_DEBRIS, true);
        debrisLandFlag = registerStateFlag(FLAG_DEBRIS_LAND, true);
        debrisDropFlag = registerStateFlag(FLAG_DEBRIS_DROP, true);
        debrisDamageFlag = registerStateFlag(FLAG_DEBRIS_DAMAGE, true);
    }

    private StateFlag registerStateFlag(String flag, boolean def) {
        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        Flag<?> existing = registry.get(flag);
        if (existing == null) {
            StateFlag stateFlag = new StateFlag(flag, def);

            registry.register(stateFlag);
            return stateFlag;
        } else {
            if (existing instanceof StateFlag) {
                return (StateFlag) existing;
            } else {
                kaboom.getLogger().severe("Could not create " + flag + " flag. Flag of a different type is already registered.");
            }
        }
        return null;
    }

    public boolean checkState(Location loc, StateFlag flag) {
        return WorldGuard.getInstance().getPlatform().getRegionContainer()
                .createQuery().testState(BukkitAdapter.adapt(loc), null, flag);
    }

    @EventHandler
    public void onDebrisLand(DebrisLandEvent e) {
        if (!checkState(e.getEntityChangeBlockEvent().getBlock().getLocation(), debrisLandFlag)) e.setCancelled(true);
    }

    @EventHandler
    public void onDebrisDrop(DebrisBreakEvent e) {
        if (e.hasDrops() && !checkState(e.getDebris().getLocation(), debrisDropFlag)) {
            e.setDrops(null);
        }
    }

    @EventHandler
    public void onDebrisDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof FallingBlock && kaboom.getHandler().isDebris(e.getDamager()) && !checkState(e.getDamager().getLocation(), debrisDamageFlag)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onExplosionHandled(HandleExplosionEvent e) {
        if (!checkState(e.getCenter(), createDebrisFlag)) {
            e.setCreatesDebris(false);
        }
    }

}
