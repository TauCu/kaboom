package me.taucu.kaboom.hooks;

import me.taucu.kaboom.KaBoom;

public abstract class Hook {

    private boolean enabled = false;
    protected final KaBoom kaboom = KaBoom.get();

    public boolean isLoaded() {
        return Hooks.getHook(this.getClass()) != null;
    }

    protected void onLoad() throws Exception {}

    protected abstract void onEnable() throws Exception;
    
    protected abstract void onDisable() throws Exception;

    public final boolean isEnabled() {
        return enabled;
    }

    public final boolean enable() {
        if (!enabled && isLoaded()) {
            try {
                onEnable();
                enabled = true;
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }

    public final boolean disable() {
        if (enabled) {
            try {
                onDisable();
                enabled = false;
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }

}
