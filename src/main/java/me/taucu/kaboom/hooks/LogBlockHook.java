package me.taucu.kaboom.hooks;

import de.diddiz.LogBlock.Actor;
import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;
import me.taucu.kaboom.events.DebrisLandEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;

public class LogBlockHook extends Hook implements Listener {

    private final Actor debrisActor = new Actor("Debris");

    private Consumer consumer;

    @Override
    public void onEnable() {
        kaboom.getLogger().info("Enabling LogBlock hook...");
        if (!Bukkit.getPluginManager().isPluginEnabled("LogBlock")) throw new IllegalStateException("LogBlock is loaded but not enabled");
        consumer = LogBlock.getInstance().getConsumer();
        Bukkit.getPluginManager().registerEvents(this, kaboom);
        kaboom.getLogger().info("LogBlock hook enabled.");
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDebrisLand(DebrisLandEvent e) {
        final EntityChangeBlockEvent ecbe = e.getEntityChangeBlockEvent();
        consumer.queueBlockPlace(debrisActor, ecbe.getBlock().getLocation(), ecbe.getBlockData());
    }

}
