package me.taucu.kaboom.commands;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.physics.ExplosionHandler;
import me.taucu.kaboom.util.LocationUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class CommandKaBoom implements CommandExecutor, TabExecutor {

    private final List<String> baseComplete = Arrays.asList("reload", "testdebris", "testexplosion");

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length > 0) {
            if (args.length == 1) {
                return StringUtil.copyPartialMatches(args[0], baseComplete, new ArrayList<String>());
            }
        } else {
            return baseComplete;
        }
        return Arrays.asList("");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "reload":
                    if (sender.hasPermission("tau.kaboom.command.reload")) {
                        KaBoom.get().loadConfig();
                        sender.sendMessage(ChatColor.AQUA + "config reloaded");
                    } else {
                        sender.sendMessage("no permission");
                    }
                    break;
                case "testdebris":
                    if (sender.hasPermission("tau.kaboom.command.testdebris")) {
                        if (sender instanceof Player) {
                            Player p = (Player) sender;
                            Block target = p.getTargetBlockExact(150, FluidCollisionMode.ALWAYS);
                            if (target == null) {
                                sender.sendMessage(ChatColor.RED + "You are not looking at a block within 150 meters");
                            } else {
                                ExplosionHandler handler = KaBoom.get().getHandler();
                                Material type = target.getType();
                                HashSet<Block> self = new HashSet<>();
                                self.add(target);
                                FallingBlock debris = handler.createDebris(LocationUtil.toCenterLocation(p.getLocation()).toVector(), target, type, self, 1.0F, handler.getBreakChanceNoFree());
                                if (debris == null) {
                                    sender.sendMessage(ChatColor.RED + "Break chance rolled true");
                                } else {
                                    sender.sendMessage(ChatColor.AQUA + "Yeeting " + type);
                                }
                            }
                        } else {
                            sender.sendMessage(ChatColor.RED + "This command may only be ran as a player");
                        }
                    } else {
                        sender.sendMessage("no permission");
                    }
                    break;
                case "testexplosion":
                    if (sender.hasPermission("tau.kaboom.command.testexplosion")) {
                        if (sender instanceof Player) {
                            Player p = (Player) sender;
                            Block target = p.getTargetBlockExact(150, FluidCollisionMode.ALWAYS);
                            if (target == null) {
                                sender.sendMessage(ChatColor.RED + "You are not looking at a block within 150 meters");
                            } else {
                                if (!target.isEmpty()) {
                                    for (BlockFace face : BlockFace.values()) {
                                        if (target.getRelative(face).isEmpty()) {
                                            target = target.getRelative(face);
                                            break;
                                        }
                                    }
                                }

                                float power = 4F;
                                if (args.length > 1) {
                                    try {
                                        power = Math.abs(Float.parseFloat(args[1]));
                                    } catch (NumberFormatException ignored) {
                                        sender.sendMessage("Power argument \"" + args[1] + "\" is not a valid float");
                                        break;
                                    }
                                }

                                if (target.getWorld().createExplosion(LocationUtil.toCenterLocation(target.getLocation()), power)) {
                                    sender.sendMessage(ChatColor.AQUA + "BOOM!");
                                } else {
                                    sender.sendMessage(ChatColor.RED + "Explosion was cancelled");
                                }
                            }
                        } else {
                            sender.sendMessage(ChatColor.RED + "This command may only be ran as a player");
                        }
                    } else {
                        sender.sendMessage("no permission");
                    }
                    break;
                default:
                    sender.sendMessage(ChatColor.RED + "invalid sub-command; try /kaboom reload");
                    break;
            }
        } else {
            sender.sendMessage(ChatColor.AQUA + "KaBoom by TauCubed " + ChatColor.GRAY + KaBoom.get().getDescription().getVersion());
            sender.sendMessage(ChatColor.GRAY + " \u2514 " + KaBoom.get().getDescription().getDescription());
            sender.sendMessage(ChatColor.AQUA + "usage: /kaboom <reload|testdebris|testexplosion>");
        }
        return true;
    }

}
