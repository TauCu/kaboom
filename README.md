# About
KaBoom is my implementation of explosion physics in Minecraft.

It works for TNT, Creepers, Fireballs and should work for any other Explosion.
It is requires a Spigot/Paper server to work.

Currently supported Minecraft versions are 1.16 - 1.19

# Features
- Silk touch option for explosions. 
- Disable or Enable debris. 
- Debris drop their block's loot tables. 
- Debris can deal damage to entities they hit. 
- Debris landing sound effects. 
- Visual Only Mode where debris will not place/drop anything. Instead dropping items at the source explosion normally. 
- Ability to set if debris should play break particles if they fail to land. 
- Ability to set whether or not to place specific or all types of debris. 
- Ability to set whether or not to drop items for specific or all types of debris. 
- Explosion physics are dynamically calculated. 
- CoreProtect support. 
- LogBlock support. 
- Prism support.


# To Use
- Stop server (if started)
- Add KaBoom binary to ./plugins
- Start server

# Configuration
- The configuration is located in ./plugins/kaboom/config.yml
- Use **/kaboom reload** to reload configuration from file. 

# Building
To build this project either use the included gitlab-ci or via `mvn clean package`

The build binaries will output to `./target/KaBoom-<version>.jar`
